/// <reference types="next/types/global" />

import * as next from 'next';

declare module '*.module.css' {}

declare module 'next' {
  type NextLayoutPage<P = {}> = NextPage<P> & {
    Layout?: 'public' | 'dashboard' | 'exhibition';
  };
}
