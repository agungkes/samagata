import Image from 'next/image';
import React, { HTMLAttributes } from 'react';
import styles from './Footer.module.css';

type Props = {
  containerClassname?: HTMLAttributes<HTMLElement>['className'];
};
const Footer: React.FC<Props> = ({ containerClassname }): JSX.Element => (
  <footer className={`${styles.FooterContainer} ${containerClassname}`}>
    <div className={styles.FooterBody}>
      <div className={styles.FooterLogo}>
        <Image src="/images/logo.png" width={30} height={34} />
        <span className="ml-4 font-medium text-lg text-white">Samagata</span>
      </div>

      <div className={styles.FooterText}>
        © {new Date().getFullYear()} Samagata. All rights reserved.
      </div>
    </div>
  </footer>
);

export default Footer;
