import { array, object, string } from 'yup';
export const CreateArtistSchema = object().shape({
  avatar: array(),
  name: string().required('Nama harus diisi'),
  action_url: string().required('Youtube url harus diisi'),
  age: string().required('Umur harus diisi'),
  artworks: array(),
  birth_place: string().required('Tempat lahir harus diisi'),
  description: string().required('Deskripsi harus diisi'),
});
