import PublicLayout from './Public';
import AdminLayout from './Dashboard';
import ExhibitionLayout from './Exhibition';

import { FC } from 'react';

const layouts = {
  public: PublicLayout,
  dashboard: AdminLayout,
  exhibition: ExhibitionLayout,
};

type LayoutWrapperProps = {
  layout: 'public' | 'dashboard' | 'exhibition';
};
const LayoutWrapper: FC<LayoutWrapperProps> = props => {
  if (!props.layout) {
    return <>{props.children}</>;
  }

  const Layout = layouts[props.layout];

  if (!Layout) {
    return <>{props.children}</>;
  }

  return <Layout {...props}>{props.children}</Layout>;
};

export default LayoutWrapper;
