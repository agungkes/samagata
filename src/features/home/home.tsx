import supabase from '@utils/supabaseClient';
import { useSession } from 'next-auth/client';
import Link from 'next/link';
import styles from './home.module.css';

const Home = (props): JSX.Element => {
  return (
    <>
      <div className="flex flex-col items-center justify-center">
        <h1 className="font-medium text-lg text-primary">
          Creativity in Inclusivity
        </h1>
        <span className="mt-4 text-center text-xs lg:text-base">
          Click one of the image below to find out more about us!
        </span>
      </div>

      <div className={styles.PageMenuContainer}>
        <Link href="/artist">
          <div className={`${styles.PageMenu} lg:-right-32`}>
            <span className={`${styles.Title} lg:mb-5 lg:mr-24`}>
              Meet Our Artist!
            </span>
            <img
              src="/images/meet-our-artist.png"
              className="lg:transform-gpu"
              width={384}
              height={343}
            />
          </div>
        </Link>

        <div className={styles.PageMenu}>
          <span className={styles.Title}>Our Exhibition!</span>
          <img
            src="/images/exhibition.png"
            width={467}
            height={336}
            className="lg:opacity-50 lg:hover:opacity-100 lg:transition-opacity lg:duration-300"
          />
        </div>

        <div className={`${styles.PageMenu} lg:-left-16 lg-top-2`}>
          <span className={`${styles.Title}`}>Let's Talk!</span>
          <img
            src="/images/lets-talk.png"
            className="lg:transform-gpu lg:opacity-50 lg:hover:opacity-100 lg:transition-opacity lg:duration-300"
            width={384}
            height={343}
          />
        </div>
      </div>
    </>
  );
};

export default Home;
