import { ExhibitionFormData } from '@features/dashboardExhibition/types';
import {
  TABLE_ARTWORK,
  TABLE_EXHIBITION,
  TABLE_EXHIBITION_HAS_ARTWORK,
} from '@utils/constants';
import supabase from '@utils/supabaseClient';
import { ArtworkDatabase } from '@utils/types/Database';
import hash from 'hash-it';

const getExtension = (name: string) => {
  const lastDot = name.lastIndexOf('.');
  return name.slice(lastDot + 1);
};

const create = async (data: ExhibitionFormData) => {
  try {
    let cover = null;

    if (data.cover) {
      const filename = `${hash({
        ...data.cover[0],
        timestamp: new Date().getTime(),
      })}.${getExtension(data.cover[0].name)}`;
      await supabase.storage.from('exhibition').upload(filename, data.cover[0]);
      const { data: exhibition_storage } = await supabase.storage
        .from('exhibition')
        .createSignedUrl(filename, 60 * 60 * 24 * 365 * 10);
      cover = exhibition_storage.signedURL;
    }

    const { data: exhibition } = await supabase.from(TABLE_EXHIBITION).insert({
      name: data.name,
      cover,
    });

    await Promise.all(
      (data.artworks || []).map(async f => {
        const filename = `${hash({
          ...f,
          timestamp: new Date().getTime(),
        })}.${getExtension(f.file.name)}`;
        await supabase.storage.from('artwork').upload(filename, f.file);
        const { data } = await supabase.storage
          .from('artwork')
          .createSignedUrl(filename, 60 * 60 * 24 * 365 * 10);
        const { data: artwork } = await supabase
          .from<ArtworkDatabase>('artwork')
          .insert({
            name: filename,
            path: data.signedURL ?? '',
          });

        await supabase.from(TABLE_EXHIBITION_HAS_ARTWORK).insert({
          exhibition_id: exhibition[0].id,
          artwork_id: artwork[0].id,
          description: f.description,
        });
      })
    );
  } catch (error) {
    console.log(`[CREATE EXHIBITION]: ${error.message}`);
    throw new Error(error.message);
  }
};

const edit = async (exhibitionId: string, data: ExhibitionFormData) => {
  try {
    await supabase
      .from(TABLE_EXHIBITION)
      .update({
        name: data.name,
      })
      .match({
        id: exhibitionId,
      });

    if (data.artworks) {
      // Remove previous artwork
      // Then insert new artwork

      await Promise.all(
        data.artworks.map(async f => {
          // Check apakah path sudah ada di database apa belum
          const hash = (await import('hash-it')).default;
          const filename = `${hash({
            ...f,
            timestamp: new Date().getTime(),
          })}.${getExtension(f.file.name)}`;
          await supabase.storage.from(TABLE_ARTWORK).upload(filename, f.file);
          const { data } = await supabase.storage
            .from(TABLE_ARTWORK)
            .createSignedUrl(filename, 60 * 60 * 24 * 365 * 10);
          const { data: artwork } = await supabase
            .from<ArtworkDatabase>(TABLE_ARTWORK)
            .insert({
              name: filename,
              path: data.signedURL,
            });
          await supabase.from(TABLE_EXHIBITION_HAS_ARTWORK).insert({
            exhibition_id: exhibitionId,
            artwork_id: artwork[0].id,
            description: f.description,
          });
        })
      );
    }
  } catch (error) {
    throw new Error(error.message);
  }
};

const remove = async (exhibitionId: string) => {
  try {
    const { data: exhibitions } = await supabase
      .from(TABLE_EXHIBITION)
      .select(
        `
        id,
        artwork (
          id,
          name
        )
      `
      )
      .match({ id: exhibitionId });

    if (exhibitions && exhibitions.length > 0) {
      const artist = exhibitions[0];
      const artworks = artist.artwork || [];

      if (artworks.length > 0) {
        await Promise.all(
          artworks.map(async artwork => {
            const { id } = artwork;
            await supabase
              .from(TABLE_EXHIBITION_HAS_ARTWORK)
              .delete()
              .match({ artwork_id: id });
            await supabase.from(TABLE_EXHIBITION).delete().match({ id: id });
          })
        );

        await supabase.storage
          .from(TABLE_EXHIBITION)
          .remove(artworks.map(e => e.name));
      }
      await supabase
        .from(TABLE_EXHIBITION)
        .delete()
        .match({ id: exhibitionId });
    }
  } catch (error) {
    throw new Error(error.message);
  }
};
export { create, edit, remove };
