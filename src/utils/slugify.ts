export default function slugify(text: string) {
  return text
    .toLowerCase()
    .replace(/[^\w\d\s]/g, '')
    .replace(/\s/g, '-');
}
