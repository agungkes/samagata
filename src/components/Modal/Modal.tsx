import clsx from 'clsx';
import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import styles from './Modal.module.css';
type ModalProps = {
  isShow?: boolean;
  onClose?: () => void;
  destroyOnClose?: boolean;
};
const Modal: React.FC<ModalProps> = ({ children, ...props }) => {
  const [isBrowser, setIsBrowser] = useState(false);

  useEffect(() => {
    setIsBrowser(true);
  }, []);

  const handleClose = e => {
    e.preventDefault();
    if (props.onClose) {
      props.onClose();
    }
  };

  const containerClassname = clsx({
    [styles.ModalContainer]: true,
    [styles.ModalShow]: props.isShow,
  });

  const modalContent = (
    <div className={containerClassname}>
      <div
        className="absolute w-full h-full bg-primary opacity-80"
        onClick={handleClose}
      />
      {props.isShow ? (
        <div className="z-10">{children}</div>
      ) : props.destroyOnClose ? null : (
        <div className="z-10">{children}</div>
      )}
    </div>
  );

  if (isBrowser) {
    return ReactDOM.createPortal(
      modalContent,
      document.getElementById('modal-root')
    );
  } else {
    return null;
  }
};

export default Modal;
