import supabase from '@utils/supabaseClient';
import { atom, selector, selectorFamily } from 'recoil';

import { Artist } from '@utils/types/Artist';
import { TABLE_ARTIST, TABLE_EXHIBITION } from '@utils/constants';
import { Exhibition, ExhibitionShow } from '@utils/types/Exhibition';

const artists = atom<Artist[]>({
  key: 'ALL_ARTISTS',
  default: [],
  effects_UNSTABLE: [
    ({ setSelf }) => {
      supabase
        .from(TABLE_ARTIST)
        .select(
          `
          id,
          name,
          description,
          age,
          location,
          avatar,
          action_url,
          artwork (
            id,
            name,
            path
          )
        `
        )
        .order('created_at')
        .then(res => {
          setSelf(res.data);
        });

      supabase
        .from(TABLE_ARTIST)
        .on('DELETE', payload => {
          const data = payload.old;
          setSelf(p => {
            const previous = p as Artist[];
            return previous.filter(f => f.id !== data.id);
          });
        })
        .on('UPDATE', () => {
          supabase
            .from(TABLE_ARTIST)
            .select(
              `
              id,
              name,
              description,
              age,
              location,
              avatar,
              action_url,
              artwork (
                id,
                name,
                path
              )
          `
            )
            .order('created_at')
            .then(res => {
              setSelf(res.data);
            });
        })
        .on('INSERT', () => {
          supabase
            .from(TABLE_ARTIST)
            .select(
              `
              id,
              name,
              description,
              age,
              location,
              avatar,
              action_url,
              artwork (
                id,
                name,
                path
              )
          `
            )
            .order('created_at')
            .then(res => {
              setSelf(res.data);
            });
        })
        .subscribe();
    },
  ],
});

export const getAllArtist = atom({
  key: 'artist_state',
  default: selector({
    key: 'CurrentUserID/Default',
    get: async ({ get }) => {
      const art = get(artists);

      return art;
    },
  }),
});

export const getArtist = selectorFamily<Artist, { name: string; id?: number }>({
  key: 'single_artist',
  get:
    ({ name, id }) =>
    ({ get }) => {
      const artists: Artist[] = get(getAllArtist) || [];

      if (artists.length > 0) {
        const artist = artists.find(f => {
          if (id) {
            return f.id === id;
          }

          return f.name === name;
        });

        if (!artist) {
          throw new Error('Id tidak ada dalam database');
        }

        return artist;
      }

      return {
        id: 0,
        avatar: '',
        name: '',
        description: '',
        age: 0,
        artwork: [{ path: '', name: '', id: 0 }],
        action_url: '',
        location: '',
      };
    },
});

export const getAllExhibition = atom<Exhibition[]>({
  key: 'GetAllExhibition',
  default: [],
  effects_UNSTABLE: [
    ({ setSelf }) => {
      supabase
        .from(TABLE_EXHIBITION)
        .select(
          `
          id,
          name,
          cover,
          artwork (
            name,
            path
          )
        `
        )
        .order('created_at')
        .then(res => {
          setSelf(res.data);
        });

      supabase
        .from(TABLE_EXHIBITION)
        .on('DELETE', payload => {
          const data = payload.old;
          setSelf(p => {
            const previous = p as Exhibition[];
            return previous.filter(f => f.id !== data.id);
          });
        })
        .on('UPDATE', () => {
          supabase
            .from(TABLE_EXHIBITION)
            .select(
              `
                id,
                name,
                artwork (
                  name,
                  path
                )
            `
            )
            .order('created_at')
            .then(res => {
              setSelf(res.data);
            });
        })
        .on('INSERT', () => {
          supabase
            .from(TABLE_EXHIBITION)
            .select(
              `
                id,
                name,
                artwork (
                  name,
                  path
                )
            `
            )
            .order('created_at')
            .then(res => {
              setSelf(res.data);
            });
        })
        .subscribe();
    },
  ],
});

export const getExhibition = selectorFamily<
  ExhibitionShow[] | undefined,
  string
>({
  key: 'SingleExhibition',
  get: id => async () => {
    const exhibition = await supabase
      .from('exhibition_has_artwork')
      .select(
        `
          id,
          description,
          name,
          artwork (
            name,
            path
          )
        `
      )
      .order('created_at')
      .match({ exhibition_id: id });

    if (exhibition.data && exhibition.data.length > 0) {
      return exhibition.data as unknown as ExhibitionShow[];
    }
    return undefined;
  },
});
export default {
  artist: getAllArtist,
  getArtist,
  getAllExhibition,
  getExhibition,
};
