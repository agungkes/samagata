import { NextLayoutPage } from 'next';
import dynamic from 'next/dynamic';

const ShowArtistPage: NextLayoutPage = dynamic(
  () => import('@features/artist/show')
);
export async function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  };
}
export async function getStaticProps() {
  // Pass post data to the page via props
  return { props: {} };
}

ShowArtistPage.Layout = 'public';
export default ShowArtistPage;
