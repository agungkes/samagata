import dynamic from 'next/dynamic';
import type { FC } from 'react';
import { Puff } from 'svg-loaders-react';

type Props = {
  show?: boolean;
  message?: string;
};

const Modal = dynamic(() => import('@components/Modal'));
const Loading: FC<Props> = ({ show, message }): JSX.Element => (
  <Modal isShow={show} destroyOnClose>
    <div className="flex flex-col items-center">
      <Puff stroke="white" />
      <span className="text-sm mt-4 text-gray-300">
        {message || 'Tunggu sebentar'}
      </span>
    </div>
  </Modal>
);

export default Loading;
