import { NextLayoutPage } from 'next';
import dynamic from 'next/dynamic';

const Show: NextLayoutPage = dynamic(() => import('@features/exhibition/show'));
Show.Layout = 'exhibition';
export default Show;
