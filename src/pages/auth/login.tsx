import dynamic from 'next/dynamic';

const LoginPage = dynamic(() => import('@features/auth/Login'));

export default LoginPage;
