const withOffline = require('next-offline')

module.exports = withOffline({
    workboxOpts: {
        swDest: process.env.NEXT_EXPORT
        ? 'service-worker.js'
        : 'static/service-worker.js',
        runtimeCaching: [
            {
                urlPattern: /.(png|jpg|jpeg)$/,
                handler: 'CacheFirst'
            },
            {
                urlPattern: /^https?.*/,
                handler: 'NetworkFirst',
                options: {
                    cacheName: 'offlineCache',
                    expiration: {
                        maxEntries: 200,
                    },
                },
            },
            {
                urlPattern: /api/,
                handler: 'NetworkFirst',
                options: {
                    cacheableResponse: {
                        statuses: [0, 200],
                    }
                }
            }
        ],
    },
    async rewrites() {
        return [
        {
            source: '/service-worker.js',
            destination: '/_next/static/service-worker.js',
        },
        ]
    },
    future: {
      webpack5: true,
    },
    webpack: function (config) {
        config.module.rules.push({
            test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
            use: {
                loader: 'url-loader',
                options: {
                    limit: 100000,
                    name: '[name].[ext]'
                }
            }
        })
        return config
    }
})