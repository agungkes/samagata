import PageTitle from '@components/UI/PageTitle';
import { getAllExhibition } from '@utils/atoms';
import routes from '@utils/routes';
import slugify from '@utils/slugify';
import Link from 'next/link';
import { useRecoilValueLoadable } from 'recoil';

const Exhibition = (): JSX.Element => {
  const exhibitions = useRecoilValueLoadable(getAllExhibition);

  if (exhibitions.state === 'hasError') {
    throw exhibitions.contents;
  }

  if (exhibitions.state === 'loading') {
    return <div>Loading...</div>;
  }

  const data = exhibitions.contents[0];

  return (
    <div>
      <PageTitle title="Our Exhibition" subtitle="Lorem ipsum dolor sit amet" />

      <div className="relative w-full h-72">
        {data && data.cover && (
          <Link href={`${routes.exhibition}/${slugify(data.name)}.${data.id}`}>
            <img src={data.cover} className="cursor-pointer" />
          </Link>
        )}
      </div>
    </div>
  );
};

export default Exhibition;
