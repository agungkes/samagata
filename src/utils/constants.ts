export const TABLE_ARTIST = 'artist';
export const TABLE_ARTWORK = 'artwork';
export const TABLE_ARTIST_HAS_ARTWORK = 'artist_has_artwork';

export const TABLE_EXHIBITION = 'exhibition';
export const TABLE_EXHIBITION_HAS_ARTWORK = 'exhibition_has_artwork';
