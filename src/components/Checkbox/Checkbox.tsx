// eslint-disable-next-line
// @ts-nocheck
import React, { forwardRef, useEffect, useRef } from 'react';

type Props = {
  label?: string;
  indeterminate?: any;
};
type Ref = HTMLInputElement;
const Checkbox = forwardRef<Ref, Props>(({ indeterminate, ...props }, ref) => {
  const defaultRef = useRef<HTMLInputElement>(null);
  const resolvedRef = ref || defaultRef;

  useEffect(() => {
    // eslint-disable-next-line
    if (resolvedRef.current) {
      // eslint-disable-next-line
      resolvedRef.current.indeterminate = indeterminate;
    }
  }, [resolvedRef, indeterminate]);
  return (
    <label className="inline-flex items-center">
      <input
        type="checkbox"
        className="form-checkbox h-5 w-5 text-primary"
        checked
        ref={resolvedRef}
        {...props}
      />
      {props.label && <span className="ml-2 text-gprimary">{props.label}</span>}
    </label>
  );
});

export default Checkbox;
