import React, { useMemo, useRef, useState } from 'react';
import {
  IoAddOutline,
  IoRemoveOutline,
  IoScanOutline,
  IoChatboxEllipsesOutline,
  IoChevronForward,
  IoChevronBack,
  IoCloseCircle,
} from 'react-icons/io5';

import type { Settings as SliderSettings } from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import dynamic from 'next/dynamic';
import { useRecoilValueLoadable } from 'recoil';
import { getExhibition } from '@utils/atoms';
import { useRouter } from 'next/router';

const Slider = dynamic(() => import('react-slick'), {
  ssr: false,
});
type Props = {};

// 110, 125, 150
const Slide = props => {
  const { index, activeIndex, zoom, imageSrc, ...restProps } = props;
  return (
    <div className="p-2" {...restProps}>
      <div
        className={`text-white flex items-center justify-center w-52 md:w-96 md:h-96 lg:h-auto cursor-pointer ${
          activeIndex === index
            ? `opacity-100 ${
                zoom === 2
                  ? 'scale-110'
                  : zoom === 3
                  ? 'scale-125'
                  : 'scale-105'
              }`
            : 'opacity-20 scale-75'
        } transition-all duration-500 transform-gpu scale`}
      >
        <img
          src={imageSrc || `/images/exhibition/${index + 1}.jpg`}
          // layout="fill"
          className="-z-10 rounded md:rounded-lg object-cover"
          style={{
            maxWidth: '100%',
            maxHeight: '100%',
          }}
        />
      </div>
    </div>
  );
};

const PrevArrow = props => {
  const { className, style, onClick } = props;

  const disabledClass =
    props.currentSlide === 0
      ? 'opacity-10 cursor-not-allowed'
      : 'opacity-100 md:opacity-50 md:hover:opacity-100';
  return (
    <IoChevronBack
      className={`${className} ${disabledClass} transition-opacity duration-300 block text-primary bg-white rounded-full left-5 z-10 h-10 w-10 md:left-10 md:h-20 md:w-20`}
      onClick={onClick}
      style={{
        ...style,
      }}
    />
  );
};

const NextArrow = props => {
  const { className, style, onClick } = props;

  const disabledClass =
    props.currentSlide === props.slideCount - 1
      ? 'opacity-10 cursor-not-allowed'
      : 'opacity-100 md:opacity-50 md:hover:opacity-100';

  return (
    <IoChevronForward
      className={`${className} ${disabledClass} transition-opacity duration-300 block text-primary bg-white rounded-full right-5 z-10 h-10 w-10 md:right-10 md:h-20 md:w-20`}
      onClick={onClick}
      style={{
        ...style,
      }}
    />
  );
};

const Popover = dynamic(
  () => import('react-tiny-popover').then(mod => mod.Popover),
  {
    ssr: false,
  }
);
const ShowExhibition: React.FC<Props> = (): JSX.Element => {
  const router = useRouter();
  const query = String(router.query.name).split('.');
  const exhibitionId = query[query.length - 1];
  const exhibition = useRecoilValueLoadable(getExhibition(exhibitionId));

  const clickMeButtonRef = useRef<HTMLButtonElement | undefined>();

  const [showDescription, setShowDescription] = useState(false);
  const [activeIndex, setActiveIndex] = useState(0);

  const [zoom, setZoom] = useState(1);

  const sliderSetting = useMemo<SliderSettings>(() => {
    return {
      centerMode: true,
      infinite: false,
      speed: 500,
      swipeToSlide: false,
      focusOnSelect: true,
      adaptiveHeight: false,
      variableWidth: true,
      dots: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            variableWidth: true,
          },
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            adaptiveHeight: false,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: false,
          },
        },
      ],
      prevArrow: <PrevArrow />,
      nextArrow: <NextArrow />,
      // afterChange: curr => setSlideIndex(curr),
      beforeChange: (currentSlide, nextSlide) => {
        setActiveIndex(nextSlide);
      },
    };
  }, []);

  if (exhibition.state === 'hasError') {
    throw exhibition.contents;
  }

  if (exhibition.state === 'loading') {
    return <div>Loading...</div>;
  }

  const data = exhibition.contents;
  const handleZoom = () => {
    if (zoom < 3) {
      setZoom(prev => prev + 1);
    }
  };
  const handleZoomOut = () => {
    if (zoom > 0) {
      setZoom(prev => prev - 1);
    }
  };

  return (
    <div className="flex-1 flex flex-col items-center justify-center bg-primary">
      <div className="w-full mx-auto mb-10">
        <Slider {...sliderSetting}>
          {data &&
            data.map((d, i) => (
              <Slide
                key={d.id}
                index={i}
                activeIndex={activeIndex}
                zoom={zoom}
                imageSrc={d.artwork.path}
              />
            ))}
        </Slider>
      </div>

      <div className="z-20 flex flex-row items-center space-x-5 absolute lg:right-40 bottom-1/4">
        <IoRemoveOutline
          className="bg-white w-8 h-8 md:w-12 md:h-12 rounded-full p-2 cursor-pointer"
          onClick={handleZoomOut}
        />
        <IoAddOutline
          className="bg-white w-8 h-8 md:w-12 md:h-12 rounded-full p-2 cursor-pointer"
          onClick={handleZoom}
        />
        <IoScanOutline className="bg-white w-8 h-8 md:w-12 md:h-12 rounded-full p-2 cursor-pointer" />

        <Popover
          isOpen={showDescription}
          positions={['top']}
          padding={20}
          align="end"
          onClickOutside={() => setShowDescription(prev => !prev)}
          ref={clickMeButtonRef} // if you'd like a ref to your popover's child, you can grab one here
          content={() => (
            <div
              className="bg-white p-3 rounded-lg max-w-md"
              onClick={() => setShowDescription(prev => !prev)}
            >
              <div className="flex flex-row items-center justify-between">
                <h3 className="text-2xl text-primary font-semibold">
                  {data && data[activeIndex].name}
                </h3>
                <IoCloseCircle
                  size={28}
                  className=" rounded text-primary cursor-pointer"
                  onClick={() => {
                    console.log('sss');
                    setShowDescription(false);
                  }}
                />
              </div>
              <p className="mt-2 leading-7 text-sm">
                {data && data[activeIndex].description}
              </p>
            </div>
          )}
        >
          <div
            onClick={() => {
              setShowDescription(prev => !prev);
            }}
          >
            <IoChatboxEllipsesOutline className="bg-white w-8 h-8 md:w-12 md:h-12 rounded-full p-2 cursor-pointer" />
          </div>
        </Popover>
      </div>

      <style jsx global>{`
        .slick-track {
          display: flex;
          align-items: center;
        }
      `}</style>
    </div>
  );
};

export default ShowExhibition;
