export type ArtistDatabase = {
  id: number;
  avatar: string;
  name: string;
  description: string;
  age: number;
  location: string;
  action_url: string;
};

export type ArtworkDatabase = {
  id: number;
  name: string;
  path: string;
};

export type ExhibitionDatabase = {
  id: number;
  name: string;
  cover: string;
};
