import { ArtistFormData } from '@features/dashboardArtist/types/Artits';
import {
  TABLE_ARTIST,
  TABLE_ARTIST_HAS_ARTWORK,
  TABLE_ARTWORK,
} from '@utils/constants';
import supabase from '@utils/supabaseClient';
import { ArtistDatabase, ArtworkDatabase } from '@utils/types/Database';
import hash from 'hash-it';

const getExtension = (name: string) => {
  const lastDot = name.lastIndexOf('.');
  return name.slice(lastDot + 1);
};

const create = async (data: ArtistFormData) => {
  try {
    let avatar = '';
    if (data.avatar && data.avatar.length > 0) {
      const filename = `${hash({
        ...data.avatar[0],
        timestamp: new Date().getTime(),
      })}.${getExtension(data.avatar[0].name)}`;
      await supabase.storage.from('avatars').upload(filename, data.avatar[0]);
      const { data: avatar_url } = await supabase.storage
        .from('avatars')
        .createSignedUrl(filename, 60 * 60 * 24 * 365 * 10);

      avatar = avatar_url.signedURL;
    }

    const { data: artist } = await supabase
      .from<ArtistDatabase>(TABLE_ARTIST)
      .insert({
        name: data.name,
        description: escape(data.description),
        age: Number(data.age),
        location: data.birth_place,
        action_url: data.action_url,
        avatar,
      });

    await Promise.all(
      (data.artworks || []).map(async f => {
        const filename = `${hash({
          ...f,
          timestamp: new Date().getTime(),
        })}.${getExtension(f.name)}`;
        await supabase.storage.from('artwork').upload(filename, f);
        const { data } = await supabase.storage
          .from('artwork')
          .createSignedUrl(filename, 60 * 60 * 24 * 365 * 10);
        const { data: artwork } = await supabase
          .from<ArtworkDatabase>('artwork')
          .insert({
            name: filename,
            path: data.signedURL,
          });
        await supabase.from('artist_has_artwork').insert({
          artist_id: artist[0].id,
          artwork_id: artwork[0].id,
        });
      })
    );
  } catch (error) {
    console.log(`[CREATE ARTIST]: ${error.message}`);
    throw new Error(error.message);
  }
};

const edit = async (artistId: string, data: ArtistFormData) => {
  try {
    const { data: artist } = await supabase
      .from<ArtistDatabase>(TABLE_ARTIST)
      .update({
        name: data.name,
        description: escape(data.description),
        age: Number(data.age),
        location: data.birth_place,
        action_url: data.action_url,
      })
      .match({
        id: artistId,
      });

    if (data.artworks) {
      // Remove previous artwork
      // Then insert new artwork

      await Promise.all(
        data.artworks.map(async f => {
          // Check apakah path sudah ada di database apa belum
          const hash = (await import('hash-it')).default;
          const filename = `${hash({
            ...f,
            timestamp: new Date().getTime(),
          })}.${getExtension(f.name)}`;
          await supabase.storage.from(TABLE_ARTWORK).upload(filename, f);
          const { data } = await supabase.storage
            .from(TABLE_ARTWORK)
            .createSignedUrl(filename, 60 * 60 * 24 * 365 * 10);
          const { data: artwork } = await supabase
            .from<ArtworkDatabase>(TABLE_ARTWORK)
            .insert({
              name: filename,
              path: data.signedURL,
            });
          await supabase.from(TABLE_ARTIST_HAS_ARTWORK).insert({
            artist_id: artist[0].id,
            artwork_id: artwork[0].id,
          });
        })
      );
    }
  } catch (error) {
    throw new Error(error.message);
  }
};

const remove = async (artistId: string) => {
  try {
    const { data: artists } = await supabase
      .from(TABLE_ARTIST)
      .select(
        `
        id,
        artwork (
          id,
          name
        )
      `
      )
      .match({ id: artistId });

    if (artists && artists.length > 0) {
      const artist = artists[0];
      const artworks = artist.artwork || [];

      if (artworks.length > 0) {
        await Promise.all(
          artworks.map(async artwork => {
            const { id } = artwork;
            await supabase
              .from(TABLE_ARTIST_HAS_ARTWORK)
              .delete()
              .match({ artwork_id: id });
            await supabase.from(TABLE_ARTWORK).delete().match({ id: id });
          })
        );

        await supabase.storage
          .from(TABLE_ARTWORK)
          .remove(artworks.map(e => e.name));
      }
      await supabase.from(TABLE_ARTIST).delete().match({ id: artistId });
    }
  } catch (error) {
    throw new Error(error.message);
  }
};
export { create, edit, remove };
