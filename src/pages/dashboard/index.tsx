import { NextLayoutPage } from 'next';
import dynamic from 'next/dynamic';

const DashboardModule: NextLayoutPage = dynamic(
  () => import('@features/dashboard')
);
DashboardModule.Layout = 'dashboard';
export default DashboardModule;
