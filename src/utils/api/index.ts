import * as artist from './artist';
import * as exhibition from './exhibition';
export default {
  artist,
  exhibition,
};
