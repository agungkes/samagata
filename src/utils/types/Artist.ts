export interface Artist {
  action_url: string;
  avatar: string;
  age: number;
  artwork: { path: string; id: string | number; name: string }[];
  description: string;
  id: number;
  location: string;
  name: string;
}
