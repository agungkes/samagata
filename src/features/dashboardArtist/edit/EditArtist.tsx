import dynamic from 'next/dynamic';
import { forwardRef, useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useRouter } from 'next/router';
import routes from '@utils/routes';
import { ArtistFormData } from '@features/dashboardArtist/types/Artits';
import api from '@utils/api';
import { CreateArtistSchema } from '@features/dashboardArtist/schema';
import { useRecoilValueLoadable } from 'recoil';
import { getArtist } from '@utils/atoms';
import { Artist } from '@utils/types/Artist';
import type { InputProps } from '@components/Form/Input/Input';
import supabase from '@utils/supabaseClient';
import { TABLE_ARTIST_HAS_ARTWORK, TABLE_ARTWORK } from '@utils/constants';

const Editor = dynamic<Record<string, unknown>>(() =>
  import('@components/Form').then(mod => mod.Editor)
);

const DynamicInput = dynamic<InputProps>(
  () => import('@components/Form/Input')
);
const Input = forwardRef<HTMLInputElement, InputProps>((props, ref) => {
  return <DynamicInput {...props} inputRef={ref} />;
});

const Uploader = dynamic(() =>
  import('@components/Form').then(mod => mod.Uploader)
);
const Loading = dynamic(() => import('@components/Loading'));

const EditArtist = (): JSX.Element => {
  const router = useRouter();
  const query = router.query;
  const artistName = query.name ?? '';
  const artistId = String(artistName).split('.')[1];
  const artist = useRecoilValueLoadable(
    getArtist({
      name: String(artistName),
      id: Number(artistId),
    })
  );
  const [isSubmitting, setSubmit] = useState(false);

  const data: Artist = artist.contents;

  const {
    register,
    handleSubmit,
    control,
    reset,
    formState: { errors },
  } = useForm<ArtistFormData>({
    resolver: yupResolver(CreateArtistSchema),
  });

  useEffect(() => {
    if (artist.state !== 'loading' && artist.state !== 'hasError') {
      const data: Artist = artist.contents;
      reset({
        name: data.name,
        age: data.age.toString(),
        birth_place: data.location,
        action_url: data.action_url,
        description: unescape(data.description),
      });
    }
  }, [artist.state]);

  if (artist.state === 'hasError') {
    throw artist.contents;
  }

  if (artist.state === 'loading') {
    return <div>Loading...</div>;
  }

  const onSubmit = async (data: ArtistFormData) => {
    try {
      setSubmit(p => !p);

      await api.artist.edit(artistId, data);

      router.push(routes.dashboard_artist);
      setSubmit(p => !p);
    } catch (error) {
      console.log(error.message);

      setSubmit(p => !p);
    }
  };

  return (
    <>
      <div className="flex flex-col pb-5">
        <form className="space-y-5" onSubmit={handleSubmit(onSubmit)}>
          <Input
            placeholder="Damar"
            label="Nama"
            errorMessage={errors.name && errors.name.message}
            disabled={isSubmitting}
            {...register('name')}
          />
          <Input
            placeholder="10 tahun"
            type="number"
            label="Umur"
            min={0}
            errorMessage={errors.age && errors.age.message}
            disabled={isSubmitting}
            {...register('age')}
          />
          <Input
            placeholder="Yogyakarta"
            label="Tempat Lahir"
            errorMessage={errors.birth_place && errors.birth_place.message}
            disabled={isSubmitting}
            defaultValue={data.location}
            {...register('birth_place')}
          />

          <div className="block">
            <span className="text-sm">Deskripsi</span>
            <Controller
              name="description"
              defaultValue={unescape(data.description)}
              control={control}
              render={({ field }) => {
                return (
                  <Editor
                    data={field.value}
                    onChange={e => {
                      field.onChange(e.editor.getData());
                    }}
                    disabled={isSubmitting}
                  />
                );
              }}
            />
            {errors.description && (
              <span className={'text-xs text-red-300'}>
                {errors.description.message}
              </span>
            )}
          </div>

          <Input
            name="action_url"
            placeholder="https://www.youtube.com/watch?v=SWDYkyJ0-v8"
            label="Youtube Url"
            errorMessage={errors.action_url && errors.action_url.message}
            disabled={isSubmitting}
            defaultValue={data.action_url}
            {...register('action_url')}
          />

          <div className="block">
            <span className="text-sm block mb-2">Artworks</span>
            <Controller
              name="artworks"
              control={control}
              render={({ field }) => {
                return (
                  <Uploader
                    accept="image/*"
                    disabled={isSubmitting}
                    onDropped={e => {
                      field.onChange(e);
                    }}
                    onDelete={async (e: any) => {
                      await supabase.storage
                        .from(TABLE_ARTWORK)
                        .remove([e.name]);
                      await supabase
                        .from(TABLE_ARTIST_HAS_ARTWORK)
                        .delete()
                        .match({ artwork_id: e.id });
                      await supabase
                        .from(TABLE_ARTWORK)
                        .delete()
                        .match({ id: e.id });
                    }}
                    defaultValue={data.artwork.map(e => ({
                      id: e.id,
                      name: e.name,
                      path: e.path,
                      preview: e.path,
                    }))}
                  />
                );
              }}
            />
            {errors.artworks && (
              <span className={'text-xs text-red-300'}>
                Artwork harus diisi
              </span>
            )}
          </div>

          <div className="block">
            <button
              className="px-5 py-3 bg-primary w-full text-white rounded"
              type="submit"
              onClick={handleSubmit(onSubmit)}
            >
              Tambahkan Artist
            </button>
          </div>
        </form>
      </div>
      <Loading show={isSubmitting} />
    </>
  );
};

export default EditArtist;
