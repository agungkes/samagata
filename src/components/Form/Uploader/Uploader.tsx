import React, { ChangeEvent, useCallback, useState } from 'react';
import { DropzoneOptions, useDropzone } from 'react-dropzone';
import { IoAdd, IoCloseCircle } from 'react-icons/io5';
import styles from './Uploader.module.css';
type UploaderProps = {
  onDropped?: (e: any) => void;
  onDelete?: (e: File) => void;
  onChangeDescription?: (e: string, idx: number) => void;
  defaultValue?: {
    name: string;
    path: string;
    preview: string;
    description?: string;
  }[];
  message?: string;
  preview?: boolean;
  withDescription?: boolean;
} & DropzoneOptions;

const Uploader: React.FC<UploaderProps> = ({
  onDropped,
  onDelete,
  defaultValue,
  message,
  preview = true,
  withDescription,
  onChangeDescription,
  ...props
}): JSX.Element => {
  const [files, setFiles] = useState(defaultValue || []);
  const onDrop = useCallback(acceptedFiles => {
    const addedFiles = acceptedFiles.map(file =>
      Object.assign(file, {
        preview: URL.createObjectURL(file),
      })
    );
    if (preview) {
      setFiles(prev => [...prev, ...addedFiles]);
    } else {
      setFiles(acceptedFiles);
    }

    if (onDropped) {
      if (withDescription) {
        const addedFilesWithDescription = [...files, ...addedFiles].map(e => ({
          file: e,
          description: '',
        }));

        onDropped(addedFilesWithDescription);
      } else {
        onDropped([...files, ...addedFiles]);
      }
    }
  }, []);

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    ...props,
  });

  const handleClickDelete = deletedFile => {
    if (onDelete) {
      onDelete(deletedFile);
    }
  };

  const handleChangeDescription =
    (fileIndex: number) => (e: ChangeEvent<HTMLTextAreaElement>) => {
      if (onChangeDescription) {
        onChangeDescription(e.target.value, fileIndex);
        // const filesWithDescription = [...files];
        // const selectedFile = filesWithDescription[fileIndex];

        // if (selectedFile) {
        //   selectedFile.description = e.target.value;
        //   setFiles(filesWithDescription);
        // }
      }
    };
  return (
    <>
      <div className={styles.UploaderContainer} {...getRootProps()}>
        <input {...getInputProps()} />
        {(files &&
          files.length > 0 &&
          !preview &&
          files.map(file => (
            <img
              key={file.name}
              src={file.preview}
              className="object-cover md:h-64"
            />
          ))) || (
          <div className="flex items-center justify-center flex-col p-10">
            <span className="text-sm text-center hidden md:block">
              {message
                ? message
                : "Drag 'n' drop some files here, or click to select files"}
            </span>
            <IoAdd size={32} className="opacity-50" />
          </div>
        )}
      </div>

      {preview && (
        <div className="flex flex-row items-center flex-wrap mt-8">
          {files.map((file, index) => (
            <div
              className="block mb-3 mr-3 relative cursor-pointer"
              key={`${file.name}__${index}`}
            >
              <IoCloseCircle
                className="absolute right-0 top-0 z-10 opacity-30 hover:opacity-100 transition-opacity duration-150"
                size={24}
                onClick={() => {
                  setFiles(prev =>
                    prev.filter(p => p.preview !== file.preview)
                  );
                  handleClickDelete(file);
                }}
              />
              <img
                key={file.name}
                src={file.preview}
                className="w-32 h-32 md:w-52 md:h-52 hover:opacity-60 transition-opacity duration-200 "
              />

              {withDescription && (
                <textarea
                  placeholder="Deskripsi"
                  onChange={handleChangeDescription(index)}
                  className="mt-2 w-52 rounded border-gray-300 shadow-sm focus:ring focus:ring-primary focus:ring-opacity-50"
                ></textarea>
              )}
            </div>
          ))}
        </div>
      )}
    </>
  );
};

export default Uploader;
