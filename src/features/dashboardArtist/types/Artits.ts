export interface ArtistFormData {
  avatar?: File[];
  name: string;
  action_url: string;
  age: string;
  artworks?: File[];
  birth_place: string;
  description: string;
}
