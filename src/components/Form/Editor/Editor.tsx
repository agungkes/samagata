import Editor from 'ckeditor4-react';

Editor.editorUrl = '/ckeditor/ckeditor.js';

export default Editor;
