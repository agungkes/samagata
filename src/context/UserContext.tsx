import React, { useEffect, useState, createContext, useContext } from 'react';
import { SupabaseClient, Session, User } from '@supabase/supabase-js';

const UserContext = createContext({ user: null, session: null });

type UserContextProviderProps = {
  supabaseClient: SupabaseClient;
};
export const UserContextProvider: React.FC<UserContextProviderProps> =
  props => {
    const { supabaseClient } = props;
    const [session, setSession] = useState<Session | null>(null);
    const [user, setUser] = useState<User | null>(null);

    useEffect(() => {
      const session = supabaseClient.auth.session();
      setSession(session);
      setUser(session?.user ?? null);
      const { data: authListener } = supabaseClient.auth.onAuthStateChange(
        async (_event, session) => {
          console.log('AUTH STATE CHANGE');

          setSession(session);
          setUser(session!.user ?? null);
        }
      );

      return () => {
        authListener.unsubscribe();
      };
    }, []);

    const value = {
      session,
      user,
    };

    return <UserContext.Provider value={value} {...props} />;
  };

export const useUser = () => {
  const context = useContext(UserContext);
  if (context === undefined) {
    throw new Error('useUser must be used within a UserContextProvider.');
  }
  return context;
};
