import Footer from '@components/Layout/components/Footer';

type Props = {};
const ExhibitionLayout: React.FC<Props> = ({ children }): JSX.Element => {
  return (
    <div className="min-h-screen flex flex-col">
      <main className="flex flex-col flex-1">{children}</main>
      <Footer containerClassname="mx-auto container" />
    </div>
  );
};

export default ExhibitionLayout;
