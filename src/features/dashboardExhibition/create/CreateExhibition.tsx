import type { InputProps } from '@components/Form/Input/Input';
import { CreateExhibitionSchema } from '@features/dashboardExhibition/schema';
import { ExhibitionFormData } from '@features/dashboardExhibition/types';
import { yupResolver } from '@hookform/resolvers/yup';
import api from '@utils/api';
import routes from '@utils/routes';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import { forwardRef, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';

const DynamicInput = dynamic<InputProps>(
  () => import('@components/Form/Input')
);
const Input = forwardRef<HTMLInputElement, InputProps>((props, ref) => {
  return <DynamicInput {...props} inputRef={ref} />;
});
const Loading = dynamic(() => import('@components/Loading'));

const Uploader = dynamic(() =>
  import('@components/Form').then(mod => mod.Uploader)
);
const CreateExhibition = () => {
  const [isSubmitting, setSubmit] = useState(false);
  const router = useRouter();
  const {
    handleSubmit,
    control,
    getValues,
    register,
    formState: { errors },
  } = useForm<ExhibitionFormData>({
    resolver: yupResolver(CreateExhibitionSchema),
  });

  const onSubmit = async (data: ExhibitionFormData) => {
    try {
      setSubmit(p => !p);

      await api.exhibition.create(data);

      router.push(routes.dashboard_exhibition.index);
      setSubmit(p => !p);
    } catch (error) {
      console.log(error.message);

      setSubmit(p => !p);
    }
  };

  return (
    <div>
      <form className="space-y-5" onSubmit={handleSubmit(onSubmit)}>
        <div>
          <Controller
            name="cover"
            control={control}
            render={({ field }) => {
              return (
                <Uploader
                  accept="image/*"
                  disabled={isSubmitting}
                  message="Cover"
                  multiple={false}
                  preview={false}
                  onDropped={e => {
                    field.onChange(e);
                  }}
                />
              );
            }}
          />
          {errors.cover && (
            <span className={'text-xs text-red-300'}>Cover harus diisi</span>
          )}
        </div>

        <Input
          placeholder="Yogyakarta Internation Exhibition"
          label="Nama Exhibition"
          errorMessage={errors.name && errors.name.message}
          disabled={isSubmitting}
          {...register('name')}
        />

        <div>
          <span className="text-lg text-primary">Artworks</span>
          <Controller
            name="artworks"
            control={control}
            render={({ field }) => {
              return (
                <Uploader
                  accept="image/*"
                  disabled={isSubmitting}
                  onDropped={e => {
                    const artworks = getValues('artworks');
                    if (artworks) {
                      const newArtworks = [...artworks, ...e];
                      field.onChange(newArtworks);
                    } else {
                      field.onChange(e);
                    }
                  }}
                  onDelete={e => {
                    const artworks = getValues('artworks');
                    if (artworks) {
                      const newArtworks = [...artworks].filter(
                        f => f.file.lastModified !== e.lastModified
                      );
                      field.onChange(newArtworks);
                    }
                  }}
                  onChangeDescription={(e, i) => {
                    const artworks = getValues('artworks');
                    artworks[i].description = e;
                    field.onChange(artworks);
                  }}
                  withDescription
                />
              );
            }}
          />
        </div>

        <div className="block">
          <button
            className="px-5 py-3 bg-primary w-full text-white rounded"
            type="submit"
            onClick={handleSubmit(onSubmit)}
          >
            Tambahkan Exhibition
          </button>
        </div>
      </form>

      <Loading show={isSubmitting} />
    </div>
  );
};

export default CreateExhibition;
