import React from 'react';
import styles from './PageTitle.module.css';

type Props = {
  title: string;
  subtitle?: string;
};
const PageTitle: React.FC<Props> = ({ title, subtitle }): JSX.Element => (
  <div className="mb-5">
    <h1 className={styles.Title}>{title}</h1>
    <span className={styles.Subtitle}>{subtitle}</span>
  </div>
);

export default PageTitle;
