import Image from 'next/image';
import { useRouter } from 'next/router';
import styles from './show.module.css';
import {
  IoChevronBackCircleOutline,
  IoChevronForwardCircleOutline,
} from 'react-icons/io5';
import Link from 'next/link';
import routes from '@utils/routes';
import { useRecoilValueLoadable } from 'recoil';
import atoms from '@utils/atoms';
import Masonry, { ResponsiveMasonry } from 'react-responsive-masonry';
import unslugify from '@utils/unslugify';

const ShowArtist = (): JSX.Element => {
  const router = useRouter();
  const query = router.query;
  const artistName = query.name ?? '';
  const artistId = String(artistName).split('.')[1];

  const artist = useRecoilValueLoadable(
    atoms.getArtist({
      name: String(artistName),
      id: Number(artistId),
    })
  );

  if (artist.state === 'hasError') {
    throw artist.contents;
  }

  if (artist.state === 'loading') {
    return <div>Loading...</div>;
  }

  const data = artist.contents;
  return (
    <>
      <div className={styles.PageTitleContainer}>
        <h1 className={styles.Title}>
          {unslugify(String(artistName).replace(/\.\d+/, ''))}
        </h1>
        <span className={styles.Info}>
          {data.age} yo. {data.location}
        </span>
      </div>

      <div className={styles.DescriptionContainer}>
        <div
          dangerouslySetInnerHTML={{
            __html: unescape(data.description),
          }}
        />
      </div>

      <div className={styles.ActionContainer}>
        <h2 className={styles.SectionTitle}>{data.name} in Action</h2>

        <div className={styles.VideoContainer}>
          <div
            className="relative overflow-hidden h-0 "
            style={{
              paddingBottom: '56.25%',
            }}
          >
            <iframe
              src={`https://www.youtube.com/embed/${data.action_url.replace(
                /.*v=/g,
                ''
              )}`}
              frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
              title="Embedded youtube"
              className="absolute w-full h-full top-0 left-0"
            />
          </div>
        </div>
      </div>

      <div className={styles.Artwork}>
        <h2 className={styles.SectionTitle}>{data.name}'s Artwork</h2>

        <ResponsiveMasonry columnsCountBreakPoints={{ 350: 1, 750: 2, 900: 3 }}>
          <Masonry columnsCount={3} gutter="10px">
            {data.artwork.map(art => (
              <a href={art.path} target="_blank" key={art.path}>
                <img src={art.path} className="w-full block cursor-pointer" />
              </a>
            ))}
          </Masonry>
        </ResponsiveMasonry>
      </div>

      <div className={styles.Navigation}>
        <Link href={routes.artist}>
          <button className={`${styles.NavigationButton} bg-primary-dark `}>
            <IoChevronBackCircleOutline size={24} className="mr-2" />
            {/* <Previous className="mr-2" /> */}
            Back to our artist
          </button>
        </Link>

        <Link href={routes.exhibition}>
          <button className={`${styles.NavigationButton} bg-primary-light`}>
            Go to exhibition
            <IoChevronForwardCircleOutline size={24} className="ml-2" />
          </button>
        </Link>
      </div>
    </>
  );
};
export default ShowArtist;
