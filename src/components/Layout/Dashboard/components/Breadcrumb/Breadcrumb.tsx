import Link from 'next/link';
import { FC, Fragment } from 'react';
import styles from './Breadcrumb.module.css';

type BreadcrumbProp = {
  pathname: string;
};
const Breadcrumb: FC<BreadcrumbProp> = ({ pathname }) => {
  return (
    <ul className={styles.Container}>
      {pathname
        .slice(1)
        .split('/')
        .map((p, i, c) => {
          const lastElem = c.length - 1 === i;

          if (lastElem) {
            return (
              <li className={'font-bold'} key={`${p}__${i}`}>
                {p.replace(/\.\d+/, '')}
              </li>
            );
          }

          return (
            <Fragment key={`${p}`}>
              <Link href={`/${c.slice(0, i + 1).join('/')}`}>
                <li className={'cursor-pointer'}>{p}</li>
              </Link>
              <span>/</span>
            </Fragment>
          );
        })}
    </ul>
  );
};

export default Breadcrumb;
