import useSidebar from '@hooks/useSidebar';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';
import styles from './Sidebar.module.css';

type Props = {
  menus: { name: string; href: string }[];
  home?: string;
};

const Sidebar: React.FC<Props> = ({ menus, home = '/' }): JSX.Element => {
  const { asPath } = useRouter();
  const { open, toggle } = useSidebar();

  const classnames = open ? 'translate-x-0' : '-translate-x-full';

  const overlayClassnames = open ? 'block' : 'hidden';

  return (
    <div className="flex">
      {/* Overlay */}
      <div
        className={`${overlayClassnames} absolute w-full h-full top-0 left-0 bg-primary-dark opacity-50 cursor-pointer transition-opacity duration-200 z-20 inset-0`}
        onClick={toggle}
      />

      <aside
        className={`${classnames} fixed top-0 left-0 h-full w-56 md:w-4/12 lg:w-3/12 xl:w-56 bg-primary inset-y-0 z-30 transform-gpu overflow-y-auto transition ease-in-out`}
      >
        <Link href={home}>
          <div className="flex items-center justify-center py-3 mt-2 hover:cursor-pointer">
            <div className="flex items-center justify-between">
              <Image src="/images/logo.png" width={30} height={34} />
              <span className="ml-3 font-medium text-lg text-white">
                Samagata
              </span>
            </div>
          </div>
        </Link>

        <nav className="mt-4 px-3">
          <ul className="space-y-2">
            {menus.map((menu, idx) => (
              <Link href={menu.href} key={`${menu.href}__${idx}`}>
                <li
                  className={`${styles.NavigationItem} ${
                    asPath === menu.href ? 'bg-primary-light' : ''
                  }`}
                >
                  <a>{menu.name}</a>
                </li>
              </Link>
            ))}
          </ul>
        </nav>
      </aside>
    </div>
  );
};

export default Sidebar;
