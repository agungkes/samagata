export default {
  home: '/',
  artist: '/artist',
  exhibition: '/exhibition',
  talk: '/lets-talk',
  dashboard: '/dashboard',
  dashboard_artist: '/dashboard/artists',
  dashboard_artist_create: '/dashboard/artists/create',
  dashboard_exhibition: {
    index: '/dashboard/exhibitions',
    create: '/dashboard/exhibitions/create',
  },
};
