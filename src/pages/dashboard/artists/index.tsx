import { NextLayoutPage } from 'next';
import dynamic from 'next/dynamic';

const DashboardArtistModule: NextLayoutPage = dynamic(
  () => import('@features/dashboardArtist')
);
DashboardArtistModule.Layout = 'dashboard';
export default DashboardArtistModule;
