import { Children, isValidElement, cloneElement } from 'react';

import { useRouter } from 'next/router';
import Link, { LinkProps } from 'next/link';
import styles from './NavigationItem.module.css';

type Props = {} & LinkProps;
const NavigationItem: React.FC<Props> = ({
  children,
  ...props
}): JSX.Element => {
  const { asPath } = useRouter();
  const child = Children.only(children);

  const className =
    asPath === props.href || asPath === props.as
      ? styles.NavigationItem
      : styles.NavigationItemActive;

  if (isValidElement(child)) {
    return (
      <Link {...props}>
        {cloneElement(child, {
          className: className || null,
        })}
      </Link>
    );
  }
  return null;
};

export default NavigationItem;
