import dynamic from 'next/dynamic';
import { forwardRef, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useRouter } from 'next/router';
import routes from '@utils/routes';
import { ArtistFormData } from '@features/dashboardArtist/types/Artits';
import api from '@utils/api';
import { CreateArtistSchema } from '@features/dashboardArtist/schema';
import { InputProps } from '@components/Form/Input/Input';

const Editor = dynamic<Record<string, unknown>>(() =>
  import('@components/Form').then(mod => mod.Editor)
);

const DynamicInput = dynamic<InputProps>(
  () => import('@components/Form/Input')
);
const Input = forwardRef<HTMLInputElement, InputProps>((props, ref) => {
  return <DynamicInput {...props} inputRef={ref} />;
});

const Uploader = dynamic(() =>
  import('@components/Form').then(mod => mod.Uploader)
);
const Loading = dynamic(() => import('@components/Loading'));

const CreateArtist = (): JSX.Element => {
  const [isSubmitting, setSubmit] = useState(false);
  const router = useRouter();
  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<ArtistFormData>({
    resolver: yupResolver(CreateArtistSchema),
  });

  const onSubmit = async (data: ArtistFormData) => {
    try {
      setSubmit(p => !p);

      await api.artist.create(data);

      router.push(routes.dashboard_artist);
    } catch (error) {
      console.log(error.message);

      setSubmit(p => !p);
    }
  };

  return (
    <>
      <div className="flex flex-col pb-5">
        <form className="space-y-5" onSubmit={handleSubmit(onSubmit)}>
          <div className="w-1/4">
            <Controller
              name="avatar"
              control={control}
              render={({ field }) => {
                return (
                  <Uploader
                    accept="image/*"
                    disabled={isSubmitting}
                    message="Avatar"
                    multiple={false}
                    preview={false}
                    onDropped={e => {
                      field.onChange(e);
                    }}
                  />
                );
              }}
            />
          </div>
          <Input
            placeholder="Damar"
            label="Nama"
            errorMessage={errors.name && errors.name.message}
            disabled={isSubmitting}
            {...register('name')}
          />
          <Input
            placeholder="10 tahun"
            type="number"
            label="Umur"
            min={0}
            errorMessage={errors.age && errors.age.message}
            disabled={isSubmitting}
            {...register('age')}
          />
          <Input
            placeholder="Yogyakarta"
            label="Tempat Lahir"
            errorMessage={errors.birth_place && errors.birth_place.message}
            disabled={isSubmitting}
            {...register('birth_place')}
          />

          <div className="block">
            <span className="text-sm">Deskripsi</span>
            <Controller
              name="description"
              defaultValue=""
              control={control}
              render={({ field }) => {
                return (
                  <Editor
                    data={field.value}
                    onChange={e => {
                      field.onChange(e.editor.getData());
                    }}
                    disabled={isSubmitting}
                  />
                );
              }}
            />
            {errors.description && (
              <span className={'text-xs text-red-300'}>
                {errors.description.message}
              </span>
            )}
          </div>

          <Input
            placeholder="https://www.youtube.com/watch?v=SWDYkyJ0-v8"
            label="Youtube Url"
            errorMessage={errors.action_url && errors.action_url.message}
            disabled={isSubmitting}
            {...register('action_url')}
          />

          <div className="block">
            <span className="text-sm block mb-2">Artworks</span>
            <Controller
              name="artworks"
              defaultValue={[]}
              control={control}
              render={({ field }) => {
                return (
                  <Uploader
                    accept="image/*"
                    multiple
                    disabled={isSubmitting}
                    onDropped={e => {
                      field.onChange(e);
                    }}
                  />
                );
              }}
            />
            {errors.artworks && (
              <span className={'text-xs text-red-300'}>
                Artwork harus diisi
              </span>
            )}
          </div>

          <div className="block">
            <button
              className="px-5 py-3 bg-primary w-full text-white rounded"
              type="submit"
              onClick={handleSubmit(onSubmit)}
            >
              Tambahkan Artist
            </button>
          </div>
        </form>
      </div>
      <Loading show={isSubmitting} />
    </>
  );
};

export default CreateArtist;
