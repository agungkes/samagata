import useSidebar from '@hooks/useSidebar';
import Image from 'next/image';
import Link from 'next/link';
import React from 'react';

type Props = {
  avatar?: {
    displayName: string;
    url: string;
  };
  containerClassname?: React.HTMLAttributes<HTMLDivElement>['className'];
};
const Header: React.FC<Props> = ({
  avatar,
  containerClassname,
}): JSX.Element => {
  const { toggle } = useSidebar();
  return (
    <header
      className={`py-4 px-6 flex items-center justify-between w-full ${containerClassname}`}
    >
      {!avatar && (
        <Link href="/">
          <div className="flex items-center cursor-pointer">
            <Image src="/images/logo.png" width={30} height={34} />
            <span className="ml-4 font-medium text-lg text-primary">
              Samagata
            </span>
          </div>
        </Link>
      )}

      <div className="cursor-pointer" onClick={toggle}>
        <svg
          width="32"
          height="32"
          viewBox="0 0 32 32"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M5.33333 24H26.6667C27.4 24 28 23.4 28 22.6667C28 21.9333 27.4 21.3333 26.6667 21.3333H5.33333C4.6 21.3333 4 21.9333 4 22.6667C4 23.4 4.6 24 5.33333 24ZM5.33333 17.3333H26.6667C27.4 17.3333 28 16.7333 28 16C28 15.2667 27.4 14.6667 26.6667 14.6667H5.33333C4.6 14.6667 4 15.2667 4 16C4 16.7333 4.6 17.3333 5.33333 17.3333ZM4 9.33333C4 10.0667 4.6 10.6667 5.33333 10.6667H26.6667C27.4 10.6667 28 10.0667 28 9.33333C28 8.6 27.4 8 26.6667 8H5.33333C4.6 8 4 8.6 4 9.33333Z"
            fill="#22185E"
          />
        </svg>
      </div>

      {avatar && (
        <div className="flex items-center space-x-3">
          <span className="text-sm">{avatar.displayName}</span>
          <div className="flex items-center justify-center text-white font-bold text-xs w-8 h-8 rounded-full bg-gray-500">
            {avatar.url === '' && avatar.displayName.split(' ').length > 1
              ? `${avatar.displayName
                  .split(' ')[0]
                  .charAt(0)}${avatar.displayName.split(' ')[1].charAt(0)}`
              : avatar.displayName.charAt(0)}
          </div>
        </div>
      )}
    </header>
  );
};

export default Header;
