import { useRouter } from 'next/router';
import routes from '@utils/routes';
import Breadcrumb from '@components/Layout/Dashboard/components/Breadcrumb';
import { FC } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import Sidebar from '@components/Layout/components/Sidebar';
import useSidebar from '@hooks/useSidebar';
import { useSession } from 'next-auth/client';
import Head from 'next/head';
type Props = {};
const MENU = [
  {
    name: 'dashboard',
    href: routes.dashboard,
  },
  {
    name: 'artist',
    href: routes.dashboard_artist,
  },
  {
    name: 'exhibition',
    href: routes.dashboard_exhibition.index,
  },
];

const DashboardLayout: FC<Props> = ({ children }): JSX.Element => {
  const { asPath } = useRouter();
  const { toggle } = useSidebar();
  const [session, loading] = useSession();

  if (loading) {
    return null;
  }

  if (!loading && !session) {
    return <div>Access Denied</div>;
  }

  const isActive = (href: string) => href === asPath;

  return (
    <>
      <Head>
        <title>
          {asPath
            .slice(1)
            .split('/')
            .map(e => e.replace(/\.\d+/, ''))
            .join(' ')
            .toLowerCase()
            .replace(/(?<= )[^\s]|^./g, a => a.toUpperCase())}{' '}
          | Samagata
        </title>
      </Head>
      <div className="flex flex-col min-h-screen bg-gray-50">
        <header className="flex flex-row justify-between p-4 bg-white">
          <div className="md:flex md:flex-row md:items-center hidden">
            <Link href="/">
              <div className="md:flex md:items-center cursor-pointer md:mr-10 hidden">
                <Image src="/images/logo.png" width={30} height={34} />
                <span className="ml-4 font-medium text-lg text-primary">
                  Samagata
                </span>
              </div>
            </Link>

            <ul className="hidden md:flex md:flex-row md:items-center md:space-x-5">
              {MENU.map(m => (
                <Link href={m.href} key={m.href}>
                  <li
                    className={`capitalize cursor-pointer ${
                      isActive(m.href) ? 'font-bold' : ''
                    }`}
                  >
                    <a>{m.name}</a>
                  </li>
                </Link>
              ))}
            </ul>
          </div>

          <div className="md:hidden cursor-pointer" onClick={toggle}>
            <svg
              width="32"
              height="32"
              viewBox="0 0 32 32"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M5.33333 24H26.6667C27.4 24 28 23.4 28 22.6667C28 21.9333 27.4 21.3333 26.6667 21.3333H5.33333C4.6 21.3333 4 21.9333 4 22.6667C4 23.4 4.6 24 5.33333 24ZM5.33333 17.3333H26.6667C27.4 17.3333 28 16.7333 28 16C28 15.2667 27.4 14.6667 26.6667 14.6667H5.33333C4.6 14.6667 4 15.2667 4 16C4 16.7333 4.6 17.3333 5.33333 17.3333ZM4 9.33333C4 10.0667 4.6 10.6667 5.33333 10.6667H26.6667C27.4 10.6667 28 10.0667 28 9.33333C28 8.6 27.4 8 26.6667 8H5.33333C4.6 8 4 8.6 4 9.33333Z"
                fill="#22185E"
              />
            </svg>
          </div>

          <div className="flex flex-row items-center space-x-3">
            <span className="text-sm">Agung Kurniawan</span>
            <div className="flex items-center justify-center bg-primary text-white text-xs rounded-full h-8 w-8">
              AK
            </div>
          </div>
        </header>

        <Sidebar menus={MENU} />

        <main className="container mx-auto flex flex-col flex-1 text-primary bg-gray-50 pt-5">
          <Breadcrumb pathname={asPath} />
          <div className="mt-5">{children}</div>
        </main>

        <footer>
          <span>Created Agung </span>
        </footer>
      </div>
    </>
  );
};

export default DashboardLayout;
