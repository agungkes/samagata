import Checkbox from '@components/Checkbox';
import React from 'react';
import {
  Column,
  useTable,
  usePagination,
  useRowSelect,
  useFlexLayout,
} from 'react-table';

type Props = {
  data: Record<string, any>[];
  columns: Column<Record<string, unknown>>[];
};

const headerProps = (props, { column }) => getStyles(props, column.align);

const cellProps = (props, { cell }) => getStyles(props, cell.column.align);

const getStyles = (props, align = 'left') => [
  props,
  {
    style: {
      justifyContent:
        align === 'right'
          ? 'flex-end'
          : align === 'center'
          ? 'center'
          : 'flex-start',
      alignItems: 'flex-start',
      display: 'flex',
    },
  },
];

const Table: React.FC<Props> = ({ columns, data }): JSX.Element => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    nextPage,
    previousPage,
    state: {
      pageIndex,
      pageSize,
      // selectedRowIds
    },
  } = useTable(
    { columns, data },
    usePagination,
    useRowSelect,
    useFlexLayout
    // hooks => {
    //   hooks.visibleColumns.push(columns => [
    //     // Let's make a column for selection
    //     {
    //       id: 'selection',
    //       Header: ({ getToggleAllPageRowsSelectedProps }) => (
    //         <Checkbox {...getToggleAllPageRowsSelectedProps()} />
    //       ),
    //       // The cell can use the individual row's getToggleRowSelectedProps method
    //       // to the render a checkbox
    //       Cell: ({ row }) => <Checkbox {...row.getToggleRowSelectedProps()} />,
    //     },
    //     ...columns,
    //   ]);
    // }
  );

  return (
    <div className="overflow-x-auto">
      <table {...getTableProps()} className="min-w-max table-auto w-full">
        <thead>
          {headerGroups.map(headerGroup => (
            <tr
              {...headerGroup.getHeaderGroupProps()}
              className="flex flex-row items-center bg-gray-200 text-gray-600 uppercase text-sm leading-normal"
            >
              {headerGroup.headers.map((column, idx) => (
                <th
                  {...column.getHeaderProps(headerProps)}
                  className={'p-6 text-left'}
                >
                  {column.render('Header')}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody
          {...getTableBodyProps()}
          className="text-gray-600 text-sm font-light"
        >
          {page.map((row, index) => {
            prepareRow(row);
            return (
              <tr
                {...row.getRowProps()}
                className={`flex items-center flex-row border-b border-gray-200 hover:bg-gray-100 ${
                  index % 2 ? 'bg-gray-100' : ''
                }`}
              >
                {row.cells.map((cell, cIdx) => {
                  return (
                    <td
                      {...cell.getCellProps(cellProps)}
                      className={'text-left p-6 whitespace-nowrap'}
                    >
                      {cell.render('Cell')}
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>

      {data.length > pageSize && (
        <div className="flex flex-col md:flex-row justify-between items-center my-12 space-y-5 md:space-y-0">
          <div className="flex flex-col">
            {/* {Object.keys(selectedRowIds).length > 0 && (
            <span>{Object.keys(selectedRowIds).length} dipilih</span>
          )} */}
            <span>
              Page{' '}
              <strong>
                {pageIndex + 1} of {pageOptions.length}
              </strong>{' '}
            </span>
          </div>

          <div className="flex text-gray-700">
            <button
              onClick={() => previousPage()}
              disabled={!canPreviousPage}
              className="h-12 w-12 mr-1 flex justify-center items-center rounded-full bg-primary text-white cursor-pointer"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="100%"
                height="100%"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
                className="feather feather-chevron-left w-6 h-6"
              >
                <polyline points="15 18 9 12 15 6"></polyline>
              </svg>
            </button>

            <div className="flex h-12 font-medium rounded-full bg-gray-100">
              <div className="w-12 h-12 md:hidden flex justify-center items-center cursor-pointer leading-5 transition duration-150 ease-in rounded-full bg-primary text-white">
                {pageIndex + 1}
              </div>
              {/* <div className="w-12 md:flex justify-center items-center hidden  cursor-pointer leading-5 transition duration-150 ease-in  rounded-full  ">
              1
            </div>
            <div className="w-12 md:flex justify-center items-center hidden  cursor-pointer leading-5 transition duration-150 ease-in  rounded-full ">
              2
            </div>
            <div className="w-12 md:flex justify-center items-center hidden  cursor-pointer leading-5 transition duration-150 ease-in  rounded-full  ">
              3
            </div>
            <div className="w-12 md:flex justify-center items-center hidden  cursor-pointer leading-5 transition duration-150 ease-in  rounded-full  ">
              ...
            </div>
            <div className="w-12 md:flex justify-center items-center hidden  cursor-pointer leading-5 transition duration-150 ease-in  rounded-full  ">
              13
            </div>
            <div className="w-12 md:flex justify-center items-center hidden  cursor-pointer leading-5 transition duration-150 ease-in  rounded-full  ">
              14
            </div>
            <div className="w-12 md:flex justify-center items-center hidden  cursor-pointer leading-5 transition duration-150 ease-in  rounded-full  ">
              15
            </div> */}
            </div>
            <button
              onClick={() => nextPage()}
              disabled={!canNextPage}
              className="h-12 w-12 ml-1 flex justify-center items-center rounded-full  bg-primary text-white cursor-pointer"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="100%"
                height="100%"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
                className="feather feather-chevron-right w-6 h-6"
              >
                <polyline points="9 18 15 12 9 6"></polyline>
              </svg>
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default Table;
