export interface Exhibition {
  artwork: {
    path: string;
    id: string | number;
    name: string;
    description: string;
  }[];
  id: number;
  cover: string;
  name: string;
}

export interface ExhibitionShow {
  id: string;
  name: string;
  description: string;
  artwork: {
    name: string;
    path: string;
  };
}
