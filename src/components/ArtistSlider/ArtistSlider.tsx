import React from 'react';

import type { Settings as SliderSettings } from 'react-slick';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import Link from 'next/link';
import dynamic from 'next/dynamic';

import PreviousArrow from './components/PrevArrow';
import NextArrow from './components/NextArrow';

import styles from './ArtistSlider.module.css';
import routes from '@utils/routes';
import slugify from '@utils/slugify';

const settings = (count: number): SliderSettings => {
  return {
    dots: true,
    dotsClass: 'slick-dots slick-dot-extended',
    customPaging: function () {
      return (
        <div>
          <style jsx>{`
            :global(.slick-dot-extended) {
              bottom: -45px !important;
            }
            :global(.slick-dots li.slick-active div) {
              background-color: rgba(34, 24, 94, 1);
            }
            :global(.slick-dots li div) {
              border-radius: 9999px;
              height: 0.75rem;
              --tw-ring-offset-shadow: var(--tw-ring-inset) 0 0 0
                var(--tw-ring-offset-width) var(--tw-ring-offset-color);
              --tw-ring-shadow: var(--tw-ring-inset) 0 0 0
                calc(2px + var(--tw-ring-offset-width)) var(--tw-ring-color);
              box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow),
                var(--tw-shadow, 0 0 #0000);
              --tw-ring-opacity: 1;
              --tw-ring-color: rgba(34, 24, 94, var(--tw-ring-opacity));
              width: 0.75rem;
            }
          `}</style>
        </div>
      );
    },
    prevArrow: <PreviousArrow className="w-10 h-10" />,
    nextArrow: <NextArrow />,
    infinite: true,
    speed: 500,
    slidesToShow: count <= 3 ? 1 : 4,
    slidesToScroll: 1,
    swipeToSlide: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: count <= 3 ? 1 : 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: count <= 3 ? 1 : 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
};

const Slider = dynamic<SliderSettings>(() => import('react-slick'), {
  ssr: false,
});

type Props = {
  artists: {
    avatar: string;
    id: string;
    name: string;
    age: string | number; // isinya umur sama ttl
    location: string;
  }[];
};

const ArtistSlider: React.FC<Props> = ({ artists }): JSX.Element => {
  return (
    <Slider {...settings(artists.length)}>
      {artists.map(artist => (
        <Link
          key={slugify(artist.name)}
          href={`${routes.artist}/${slugify(artist.name)}.${artist.id}`}
        >
          <div className={styles.SliderItemContainer}>
            <div className={styles.SliderItem}>
              <img src={artist.avatar} className="w-full h-full rounded-full" />
              {/* <h3 className={styles.SliderText}>{artist.name}</h3> */}
            </div>

            <div className={styles.SliderInfoContainer}>
              <p className={styles.ArtistName}>{artist.name}</p>
              <span className={styles.ArtistInfo}>
                {artist.age}. {artist.location}
              </span>
            </div>
          </div>
        </Link>
      ))}
    </Slider>
  );
};

export default ArtistSlider;
