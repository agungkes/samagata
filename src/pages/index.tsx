import { NextLayoutPage } from 'next';
import dynamic from 'next/dynamic';

const HomePage = dynamic(() => import('@features/home'));
const Home: NextLayoutPage = () => <HomePage />;

Home.Layout = 'public';

export default Home;
