import Table from '@components/Table';
import api from '@utils/api';
import atoms from '@utils/atoms';
import routes from '@utils/routes';
import { ExhibitionDatabase } from '@utils/types/Database';
import Link from 'next/link';
import { useMemo } from 'react';
import { IoTrashOutline } from 'react-icons/io5';
import { useRecoilValueLoadable } from 'recoil';

const DashboardExhibition = () => {
  const dataExhibitions = useRecoilValueLoadable(atoms.getAllExhibition);
  const columns = useMemo(
    () => [
      {
        Header: 'Name',
        accessor: 'name',
      },
      {
        Header: 'Action',
        accessor: '',
        align: 'center',
        Cell: ({
          row: { original },
        }: {
          row: { original: ExhibitionDatabase };
        }) => {
          return (
            <div className="flex flex-row items-center space-x-2">
              {/* <Link href={''}>
                <button className="p-2 rounded text-primary hover:bg-primary hover:text-white duration-200 transition-colors focus:ring-primary">
                  <IoPencil />
                </button>
              </Link> */}
              <button
                onClick={async () => {
                  await api.exhibition.remove(String(original.id));
                }}
                className="text-red-500 p-2 rounded  hover:bg-red-500 hover:text-white duration-200 transition-colors focus:ring-red-500"
              >
                <IoTrashOutline />
              </button>
            </div>
          );
        },
      },
    ],
    []
  );

  if (dataExhibitions.state === 'hasError') {
    throw dataExhibitions.contents;
  }

  if (dataExhibitions.state === 'loading') {
    return <div>Loading...</div>;
  }

  return (
    <div className="flex flex-col">
      <div className="mb-3 self-end">
        <Link href={routes.dashboard_exhibition.create}>
          <button className="px-5 py-3 bg-primary rounded text-white hover:bg-primary-light transition-colors duration-300 ease-in">
            Tambahkan Baru
          </button>
        </Link>
      </div>
      <Table columns={columns} data={dataExhibitions.contents} />
    </div>
  );
};

export default DashboardExhibition;
