import { array, object, string } from 'yup';
export const CreateExhibitionSchema = object().shape({
  name: string().required('Nama exhibition harus diisi'),
  cover: array(),
  artwork: array(),
});
