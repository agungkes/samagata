import { NextLayoutPage } from 'next';
import dynamic from 'next/dynamic';

const Page: NextLayoutPage = dynamic(
  () => import('@features/dashboardExhibition/create')
);
Page.Layout = 'dashboard';

export default Page;
