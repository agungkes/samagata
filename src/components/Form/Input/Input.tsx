import { DetailedHTMLProps, ForwardedRef, InputHTMLAttributes } from 'react';
import styles from './Input.module.css';
import clsx from 'clsx';

export type InputProps = {
  label?: string;
  isValid?: boolean;
  errorMessage?: string;
  inputRef?: ForwardedRef<HTMLInputElement>;
} & DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>;

const Input = (props): JSX.Element => {
  const { label, errorMessage, isValid, inputRef, ...inputProps } = props;

  const fallbackMessageClassname = clsx({
    [styles.FallbackMessage]: true,
    [styles.FallbackMessageError]: errorMessage || false,
  });

  const inputClassname = clsx({
    [styles.Input]: true,
    [styles.InputError]: isValid === false || errorMessage,
    [styles.InputSuccess]: isValid,
    [styles.InputDisable]: props.disabled || props.readOnly,
  });

  return (
    <div className="block">
      {label && <label className="text-sm">{label}</label>}
      <input
        type="text"
        aria-invalid={errorMessage ? 'true' : 'false'}
        className={inputClassname}
        {...inputProps}
        ref={inputRef}
      />
      <span className={fallbackMessageClassname}>{errorMessage}</span>
    </div>
  );
};

export default Input;
