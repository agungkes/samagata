export type ExhibitionFormData = {
  name: string;
  cover: File[];
  artworks?: {
    file: File;
    description: string;
  }[];
};
