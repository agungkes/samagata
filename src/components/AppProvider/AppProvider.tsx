import React from 'react';
import { Provider } from 'next-auth/client';
import { Session } from 'next-auth';
import { RecoilRoot } from 'recoil';

type Props = {
  session: Session;
};
const AppProvider: React.FC<Props> = ({ children, session }): JSX.Element => (
  <RecoilRoot>
    <Provider session={session}>{children}</Provider>
  </RecoilRoot>
);

export default AppProvider;
