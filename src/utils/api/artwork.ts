import { TABLE_ARTIST_HAS_ARTWORK, TABLE_ARTWORK } from '@utils/constants';
import supabase from '@utils/supabaseClient';

const remove = async ({ name, artworkId, artistId }) => {
  try {
    const { data: previousImage } = await supabase
      .from(TABLE_ARTIST_HAS_ARTWORK)
      .select(
        `
        id,
        artwork_id
        `
      )
      .match({ artist_id: artistId, artwork_id: artworkId });

    if (previousImage && previousImage.length > 0) {
      await supabase.storage.from(TABLE_ARTWORK).remove([name]);
      await supabase
        .from(TABLE_ARTIST_HAS_ARTWORK)
        .delete()
        .match({ id: previousImage[0].id });
      await supabase
        .from(TABLE_ARTWORK)
        .delete()
        .match({ id: previousImage[0].artwork_id });
    }
    throw new Error(
      `Tidak ditemukan artis(${artistId}) yang memiliki artwork(${artworkId})}`
    );
  } catch (error) {
    throw new Error(error.message);
  }
};

export { remove };
