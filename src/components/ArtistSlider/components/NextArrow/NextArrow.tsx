import React from 'react';

const NextArrow: React.FC<React.SVGProps<SVGElement>> = props => {
  const { className, style, onClick } = props;
  return (
    <svg
      width="56"
      height="56"
      viewBox="0 0 56 56"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={className}
      style={{
        ...style,
        display: 'block',
        width: 45,
        height: 45,
        zIndex: 20,
        top: '45%',
        right: '-10px',
      }}
      onClick={onClick}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M28 0C12.536 0 0 12.536 0 28C0 43.464 12.536 56 28 56C43.464 56 56 43.464 56 28C56 12.536 43.464 0 28 0ZM21.7233 18.9467C20.8133 18.0367 20.8133 16.5667 21.7233 15.6567C22.6333 14.7467 24.1267 14.7467 25.0133 15.6333L35.7233 26.3433C36.6333 27.2533 36.6333 28.7233 35.7233 29.6333L25.0133 40.3433C24.1033 41.2533 22.6333 41.2533 21.7233 40.3433C20.8133 39.4333 20.8133 37.9633 21.7233 37.0533L30.7767 28L21.7233 18.9467Z"
        fill="#A90070"
      />
    </svg>
  );
};

export default NextArrow;
