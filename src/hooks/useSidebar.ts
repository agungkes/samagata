import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { atom, useRecoilState } from 'recoil';

const sidebarState = atom({
  key: 'sidebarState',
  default: false,
});
const useSidebar = () => {
  const router = useRouter();
  const [open, setOpen] = useRecoilState(sidebarState);
  const toggleSidebar = () => setOpen(prev => !prev);

  useEffect(() => {
    const handleCloseSidebar = () => {
      setOpen(false);
    };

    if (open) {
      router.events.on('routeChangeStart', handleCloseSidebar);
    } else {
      router.events.off('routeChangeStart', handleCloseSidebar);
    }

    return () => {
      router.events.off('routeChangeStart', handleCloseSidebar);
    };
  }, [open]);

  return {
    toggle: toggleSidebar,
    open,
  };
};

export default useSidebar;
