import { NextLayoutPage } from 'next';
import dynamic from 'next/dynamic';

const ArtistModule: NextLayoutPage = dynamic(() => import('@features/artist'));

ArtistModule.Layout = 'public';
export default ArtistModule;
