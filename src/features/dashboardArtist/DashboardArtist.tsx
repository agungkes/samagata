import Table from '@components/Table';
import api from '@utils/api';
import atoms from '@utils/atoms';
import routes from '@utils/routes';
import slugify from '@utils/slugify';
import { ArtistDatabase, ArtworkDatabase } from '@utils/types/Database';
import Link from 'next/link';
import { useMemo } from 'react';
import { IoPencil, IoTrashOutline } from 'react-icons/io5';
import { useRecoilValueLoadable } from 'recoil';

const DashboardArtist = () => {
  const artists = useRecoilValueLoadable<ArtistDatabase[]>(atoms.artist);
  const columns = useMemo(
    () => [
      {
        Header: 'Name',
        accessor: 'name',
      },
      {
        Header: 'Age',
        accessor: 'age',
        align: 'center',
      },
      {
        Header: 'Location',
        accessor: 'location',
      },
      {
        Header: 'Artwork',
        accessor: 'artwork',
        Cell: ({ value }: { value: ArtworkDatabase[] }) => {
          return value.slice(0, 3).map((d, i) => (
            <a target="_blank" key={d.path} href={d.path}>
              <img
                src={d.path}
                className={`w-10 h-10 rounded-full border-2 cursor-pointer border-white relative ${
                  i === 0 ? '' : `-left-${i * 4}`
                }`}
              />
            </a>
          ));
        },
      },
      {
        Header: 'Action',
        accessor: '',
        align: 'center',
        Cell: ({
          row: { original },
        }: {
          row: { original: ArtistDatabase };
        }) => {
          const href = `${routes.dashboard_artist}/${slugify(original.name)}.${
            original.id
          }`;
          return (
            <div className="flex flex-row items-center space-x-2">
              <Link href={`${href}?artistId=${original.id}`} as={href}>
                <button className="p-2 rounded text-primary hover:bg-primary hover:text-white duration-200 transition-colors focus:ring-primary">
                  <IoPencil />
                </button>
              </Link>
              <button
                onClick={async () => {
                  await api.artist.remove(String(original.id));
                }}
                className="text-red-500 p-2 rounded  hover:bg-red-500 hover:text-white duration-200 transition-colors focus:ring-red-500"
              >
                <IoTrashOutline />
              </button>
            </div>
          );
        },
      },
    ],
    []
  );

  if (artists.state === 'hasError') {
    throw artists.contents;
  }

  if (artists.state === 'loading') {
    return <div>Loading...</div>;
  }

  return (
    <div className="flex flex-col">
      <div className="mb-3 self-end">
        <Link href={routes.dashboard_artist_create}>
          <button className="px-5 py-3 bg-primary rounded text-white hover:bg-primary-light transition-colors duration-300 ease-in">
            Tambahkan Baru
          </button>
        </Link>
      </div>
      <Table columns={columns} data={artists.contents} />
    </div>
  );
};

export default DashboardArtist;
