const unslugify = (slug: string) => slug.replace(/-/g, ' ');

export default unslugify;
