import { NextLayoutPage } from 'next';
import dynamic from 'next/dynamic';

const EditArtistPage: NextLayoutPage = dynamic(
  () => import('@features/dashboardArtist/edit')
);
EditArtistPage.Layout = 'dashboard';

export default EditArtistPage;
