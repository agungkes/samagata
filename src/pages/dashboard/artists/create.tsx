import { NextLayoutPage } from 'next';
import dynamic from 'next/dynamic';

const CreateArtistPage: NextLayoutPage = dynamic(
  () => import('@features/dashboardArtist/create')
);
CreateArtistPage.Layout = 'dashboard';

export default CreateArtistPage;
