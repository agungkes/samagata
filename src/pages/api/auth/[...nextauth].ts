import supabase from '@utils/supabaseClient';
import NextAuth from 'next-auth';
import Providers from 'next-auth/providers';

export default NextAuth({
  providers: [
    Providers.Credentials({
      name: 'Credentials',
      credentials: {
        email: {
          label: 'Email',
          type: 'email',
          placeholder: 'super@samagata.com',
        },
        password: {
          label: 'Password',
          type: 'password',
          placeholder: 'supersecretpassword',
        },
      },
      async authorize(credentials: { email: string; password: string }) {
        const { user } = await supabase.auth.signIn({
          email: credentials.email,
          password: credentials.password,
        });

        if (user) {
          return {
            name: user.email,
            email: user.email,
          };
        }

        return null;
      },
    }),
  ],
  jwt: {
    signingKey:
      '{"kty":"oct","kid":"Fl7Ql55ZvCKk2Cfnj1xcZtme78fE9MCxvq3mtoYI3SY","alg":"HS512","k":"rauU57uUGG6mdcrwOc_zpqg-dfOKSK1_bmz5vqCNVk8"}',
    verificationOptions: {
      algorithms: ['HS512'],
    },
  },
  session: {
    jwt: true,
    maxAge: 30 * 24 * 60 * 60, // 30 days
  },
  callbacks: {
    session: async session => {
      return {
        ...session,
      };
    },
  },
  pages: {
    signIn: '/auth/login',
  },
});
