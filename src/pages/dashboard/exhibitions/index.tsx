import { NextLayoutPage } from 'next';
import dynamic from 'next/dynamic';

const DashboardExhibition: NextLayoutPage = dynamic(
  () => import('@features/dashboardExhibition')
);
DashboardExhibition.Layout = 'dashboard';
export default DashboardExhibition;
