import Footer from '@components/Layout/components/Footer';
import Header from '@components/Layout/components/Header';
import Sidebar from '@components/Layout/components/Sidebar';
import routes from '@utils/routes';

type Props = {};
const MENU = [
  {
    name: 'Meet Our Artist',
    href: routes.artist,
  },
  {
    name: 'Exhibition',
    href: routes.exhibition,
  },
  {
    name: "Let's Talk",
    href: routes.talk,
  },
];
const PublicLayout: React.FC<Props> = ({ children }): JSX.Element => {
  return (
    <div className="container min-h-screen flex flex-col mx-auto">
      <Header />
      <Sidebar menus={MENU} />
      <main className="flex flex-col flex-1 my-6 px-6">{children}</main>
      <Footer />
    </div>
  );
};

export default PublicLayout;
