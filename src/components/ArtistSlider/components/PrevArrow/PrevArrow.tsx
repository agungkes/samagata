import React from 'react';

const PreviousArrow: React.FC<React.SVGProps<SVGElement>> = props => {
  const { className, style, onClick } = props;
  return (
    <svg
      width="56"
      height="56"
      viewBox="0 0 56 56"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={className}
      style={{
        ...style,
        display: 'block',
        width: 45,
        height: 45,
        zIndex: 20,
        top: '45%',
        left: '-10px',
      }}
      onClick={onClick}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M28 56C43.464 56 56 43.464 56 28C56 12.536 43.464 0 28 0C12.536 0 0 12.536 0 28C0 43.464 12.536 56 28 56ZM34.2767 37.0533C35.1867 37.9633 35.1867 39.4333 34.2767 40.3433C33.3667 41.2533 31.8733 41.2533 30.9867 40.3667L20.2767 29.6567C19.3667 28.7467 19.3667 27.2767 20.2767 26.3667L30.9867 15.6567C31.8967 14.7467 33.3667 14.7467 34.2767 15.6567C35.1867 16.5667 35.1867 18.0367 34.2767 18.9467L25.2233 28L34.2767 37.0533Z"
        fill="#A90070"
      />
    </svg>
  );
};

export default PreviousArrow;
