import { NextLayoutPage } from 'next';
import dynamic from 'next/dynamic';

const ExhibitionModule: NextLayoutPage = dynamic(
  () => import('@features/exhibition')
);

ExhibitionModule.Layout = 'public';
export default ExhibitionModule;
