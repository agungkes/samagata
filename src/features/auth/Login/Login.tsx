import { Input } from '@components/Form';
import routes from '@utils/routes';
import { getCsrfToken, signIn } from 'next-auth/client';
import { useRouter } from 'next/router';
import { useState } from 'react';

const Login = () => {
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState('demo@datains.id');
  const [password, setPassword] = useState('Admin123');

  const handleLogin = async data => {
    try {
      setLoading(true);
      const loggedIn = await signIn('credentials', {
        email: data.email,
        password: data.password,
        redirect: false,
      });
      if (loggedIn.error) {
        throw new Error(loggedIn.error);
      }

      router.push(routes.dashboard);
    } catch (error) {
      alert(error.error_description || error.message);
    } finally {
      setLoading(false);
    }
  };
  return (
    <div className="row flex flex-center">
      <div className="col-6 form-widget">
        <h1 className="header">Samagata</h1>

        <Input
          placeholder="example@domain.com"
          type="email"
          value={email}
          onChange={e => setEmail(e.target.value)}
        />
        <Input
          placeholder="supersecretpassword"
          value={password}
          type="password"
          onChange={e => setPassword(e.target.value)}
        />
        <div className="mt-5">
          <button
            onClick={e => {
              e.preventDefault();
              handleLogin({ email, password });
            }}
            className={'button block bg-primary p-3 text-white rounded'}
            disabled={loading}
          >
            {loading ? <span>Loading</span> : <span>Send magic link</span>}
          </button>
        </div>
      </div>
    </div>
  );
};

export async function getServerSideProps(context) {
  return {
    props: {
      csrfToken: await getCsrfToken(context),
    },
  };
}
export default Login;
