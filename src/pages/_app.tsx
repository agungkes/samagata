import AppProvider from '@components/AppProvider';
import '../styles/globals.css';
import Head from 'next/head';
import type { Session } from 'next-auth';
import type { AppProps } from 'next/app';
import LayoutWrapper from '@components/Layout/LayoutWrapper';
import { NextLayoutPage } from 'next';
import NextNprogress from 'nextjs-progressbar';

type MyAppProps = {
  session: Session;
};
type AppLayoutProps<P = {}> = AppProps<P> & {
  Component: NextLayoutPage;
};

function MyApp({ Component, pageProps }: AppLayoutProps<MyAppProps>) {
  return (
    <AppProvider session={pageProps.session}>
      <Head>
        <title>Samagata</title>
        <meta name="theme-color" content="#22185E" />

        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta
          name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"
        />
        <meta name="description" content="Samagata" />
        <meta name="keywords" content="samagata" />
      </Head>

      <LayoutWrapper layout={Component.Layout}>
        <Component {...pageProps} />
        <NextNprogress
          color="#22185E"
          startPosition={0}
          stopDelayMs={200}
          height={2}
        />
      </LayoutWrapper>
    </AppProvider>
  );
}

export default MyApp;
