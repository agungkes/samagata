import NextAuth from 'next-auth';

interface User {
  id: string;
  role: string;
  email: string;
}
declare module 'next-auth' {
  /**
   * Returned by `useSession`, `getSession` and received as a prop on the `Provider` React Context
   */
  interface Session {
    user: {
      address: string;
      role: string;
      email: string;
      name: string;
    };
  }
}
